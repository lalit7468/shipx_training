-- DROP DATABASE `company`;

CREATE DATABASE `company`;
USE company;


CREATE TABLE departments (
	department_id INT PRIMARY KEY,
    department_name VARCHAR(255),
    dep_group VARCHAR(255)
    );
CREATE TABLE employees (
	employee_id INT NOT NULL,
    emp_name VARCHAR(255),
    city VARCHAR(255),
    state VARCHAR(255),
    age INT,
    department_id INT NOT NULL,
    PRIMARY KEY (employee_id),
    FOREIGN KEY (department_id) REFERENCES departments (department_id)
    );










INSERT INTO departments
	VALUES
    (1, 'Development', 'Engineering'),
    (2, 'QA', 'Engineering'),
    (3, 'Consulting', 'Services'),
    (4, 'Support', 'Services'),
    (5, 'Support', 'Sales');



INSERT INTO employees 
VALUES
	(1, 'Ashish', 'Bengaluru','Karnataka',25,1),
	(2, 'Bindu', 'Chennai', 'Tamil Nadu',	37, 2),
	(3, 'Chethan', 'Hyderabad', 'Telangana', 39, 1),
	(4, 'Danny', 'Bengaluru', 'Karnataka', 26, 1),
	(5, 'Eva', 'Chennai', 'Tamil Nadu', 27, 2),
	(6, 'Fatima', 'Hyderabad', 'Telangana', 25, 3),
	(7, 'Gaurav', 'Mumbai', 'Maharashtra', 29, 2),
	(8, 'HImesh', 'New Delhi', 'Delhi', 31, 1),
	(9, 'Indu', 'Bengaluru', 'Karnataka',	26, 2),
	(10, 'Jayan', 'Chennai', 'Tamil Nadu', 28, 1),
	(11, 'Ketan', 'Mumbai', 'Maharashtra', 29, 3),
	(12, 'Lisa', 'Mysuru', 'Karnataka', 31, 4),
	(13, 'Manu', 'Mysuru', 'Karnataka', 27, 1),
	(14, 'Nandini', 'Chennai', 'Tamil Nadu', 30, 3),
	(15, 'Osman', 'New Delhi', 'Delhi', 32, 4),
	(16, 'Peter', 'Bengaluru', 'Karnataka', 35, 1);
    

-- // List employee id, name, city, state for all employees
SELECT employee_id, emp_name, city, state FROM employees;





-- //List employee name for all employees older than 30 years
SELECT emp_name FROM employees
WHERE age > 30;





-- // List employee name for employees in Bengaluru, Karnataka (city, state)
SELECT emp_name FROM employees
WHERE city = 'Bengaluru' AND state = 'Karnataka';




-- //List city, state, average age of employee for all city-state combinations

SELECT city, state, AVG(age) FROM employees
GROUP BY city, state;


-- // List city and count of employees for Karnataka State
SELECT city, COUNT(employee_id) FROM employees
WHERE state = 'Karnataka'
GROUP BY city;

-- // List city and count of employees for all cities with at least 3 employees
SELECT city, COUNT(employee_id) FROM employees
GROUP BY city
HAVING COUNT(employee_id) >=3;

-- // List employee name, department name, group name for all employees
SELECT emp_name, d.department_name, d.dep_group FROM employees AS e
INNER JOIN departments as d
ON e.department_id = d.department_id;










-- // List employee name, age for all employee in Engineering Group
SELECT emp_name, age FROM employees e
JOIN departments d
USING (department_id)
WHERE d.dep_group = 'Engineering';




-- // List departments that does not have any employees

SELECT department_name,dep_group FROM departments
WHERE department_id NOT IN (
					SELECT DISTINCT(department_id) FROM employees);











-- // List department name, maximum age of employees (for that department) for all departments (if no employees in a dept, max age should be NULL or 0)
SELECT department_name, MAX(e.age) FROM departments
JOIN employees e
USING (department_id)
GROUP BY department_name
HAVING MAX(e.age);





-- // Add new column pincode varchar(255) to employees tables 
ALTER TABLE employees
ADD pincode VARCHAR(255);

SELECT * FROM employees;




-- //Change the size of name column in employees table to 512
ALTER TABLE employees
MODIFY emp_name VARCHAR(512);



-- // create a non unique index on employees tables on columns (city, state)

-- // by default columns are non-unique. 


















































































    
    
    

