USE sql_store;

SELECT * FROM customers;

SELECT * FROM customers WHERE customer_id=1;

SELECT * FROM customers ORDER BY first_name; 

-- //access column
SELECT 
	last_name, 
    first_name, 
    points, 
    points*10 + 100 AS 'discount_factor'
FROM customers; 

-- //duplicate clause use DISTINCT
SELECT state FROM customers;
SELECT DISTINCT state FROM customers;

-- // excercise 
SELECT name, unit_price, unit_price * 1.1 AS 'new price' FROM products;

-- // where clause
SELECT * FROM customers WHERE points > 3000;
SELECT * FROM customers WHERE state = 'VA';
SELECT * FROM customers WHERE state <> 'VA';

SELECT * FROM customers WHERE birth_date > '1990-01-01';

-- // excercise get the orders placed this year
SELECT * FROM orders WHERE order_date >= '2019-01-01';

-- // AND Clause 
SELECT * FROM customers
WHERE birth_date > '1990-01-01' AND points > 1000;

-- // OR Clause 
SELECT * FROM customers
WHERE birth_date > '1990-01-01' OR points > 1000;

-- AND > OR in order to priority

-- // NOT  clause
SELECT * FROM customers
WHERE NOT (birth_date > '1990-01-01');

-- // Exercise :- from the order_items table, get the items for order #6, where the 
-- // the total price is greater than 30

SELECT * FROM order_items
WHERE order_id = 6 AND unit_price * quantity >30;


-- // IN clause
SELECT * FROM customers
-- WHERE state = 'VA' OR state='GA' or state='FL' 
WHERE state IN ('VA','FL','GA');


-- //NOT IN clause
SELECT * FROM customers
-- WHERE state = 'VA' OR state='GA' or state='FL' 
WHERE state NOT IN ('VA','FL','GA');

-- // return products with  quantity in stock equal to 49, 38, 72
SELECT * FROM products
WHERE quantity_in_stock IN (49,38,72);

-- // BETWEEN operator
SELECT * FROM customers
-- WHERE points >= 1000 AND points <= 3000
WHERE points BETWEEN 1000 AND 3000;

-- // LIKE operator
-- // underscore means one character and percentage means unlimited characters
SELECT * FROM customers
-- WHERE last_name LIKE 'b%';
WHERE last_name LIKE '%y';

SELECT * FROM customers
WHERE last_name LIKE '_____y';

 -- // REGEXP operator 
 SELECT * FROM customers
 -- WHERE last_name LIKE '%field%';
 WHERE last_name REGEXP 'field';
 
 -- // first word must start => then we use '^'
 -- // last word must end => then we use '$'
SELECT * FROM customers
 WHERE last_name REGEXP '^mac';

SELECT * FROM customers
WHERE last_name REGEXP 'field$';
 
 
 -- // find multiple words
  SELECT * FROM customers
 WHERE last_name REGEXP 'field|mac|rose';
 -- // | logical or
 -- // [abcd]
 -- // [a-h]  for range

-- // EXercise
-- // first names are ELKA or AMBUR
SELECT * FROM customers
WHERE first_name REGEXP 'ELKA|AMBUR';

-- // last names end with EY or ON
SELECT * FROM customers
WHERE last_name REGEXP 'EY$|ON$';

-- // last names start with MY or contains SE
SELECT * FROM customers
WHERE last_name REGEXP '^MY|SE';

-- // last names contain B followed by R or U
SELECT * FROM customers
WHERE last_name REGEXP 'B[RU]'; 
 
 
-- // IS NULL operator
SELECT * FROM customers
WHERE phone IS NULL;

-- // IS NOT NULL operator
SELECT * FROM customers
WHERE phone IS NOT NULL;

-- // order by clause
SELECT * FROM customers
ORDER BY first_name;

-- // order by in descending order
SELECT * FROM customers
ORDER BY first_name DESC;

-- // multiple column sort
SELECT * FROM customers
ORDER BY first_name, state;

-- // multiple column sort
SELECT * FROM customers
ORDER BY state DESC, first_name;

-- // exercise
-- SELECT *, quantity*unit_price AS total_price
-- FROM order_items
-- WHERE order_id = 2
-- ORDER BY quantity*unit_price DESC;

-- // LIMIT clause
SELECT * FROM customers
LIMIT 3;

-- //  exercise get the top three loyal customers
SELECT * FROM customers ORDER BY points DESC
LIMIT 3;

-- // INNER JOIN we can write JOIN OR INNER JOIN both name are valid
SELECT order_id, first_name, last_name, orders.customer_id FROM orders
JOIN customers ON orders.customer_id = customers.customer_id;


 -- // joining across databases table
 -- // like we have sql_store and sql_inventory database and we want to join sql_store orders table with
 -- // sql_inventory products table
 
 SELECT * FROM order_items oi
 JOIN sql_inventory.products p
	ON oi.product_id=p.product_id;
 
 -- // SELF JOIN.......................................
 
USE sql_hr;
 
SELECT 
	e.employee_id,
    e.first_name,
    m.first_name AS manager
FROM employees e
JOIN employees m
	ON e.reports_to = m.employee_id;
    
    
    
-- // JOINING multiple tables like 3 tables
USE sql_store;

SELECT 
	o.order_id,
    o.order_date,
    c.first_name,
    c.last_name,
    os.name AS status
FROM orders o
JOIN customers c
	ON o.customer_id = c.customer_id
JOIN order_statuses os
	ON o.status = os.order_status_id;
-- //.......................................
USE sql_invoicing;

SELECT * FROM payments p
JOIN clients c ON p.client_id = c.client_id
JOIN payment_methods pm
	ON p.payment_method = pm.payment_method_id;
    
    
-- // Compound join=> multiple conditions to join tables
SELECT * FROM order_items oi
JOIN order_item_notes oin
	ON oi.order_id = oin.order_Id
    AND oi.product_id = oin.product_id;
    
USE sql_store;
SELECT
	c.customer_id,
	c.first_name,
	o.order_id
FROM customers c
LEFT JOIN orders o
	ON c.customer_id = o.customer_id
ORDER BY c.customer_id;


SELECT
	c.customer_id,
	c.first_name,
	o.order_id
FROM customers c
RIGHT JOIN orders o
	ON c.customer_id = o.customer_id
ORDER BY c.customer_id;


-- // USING clause
-- it is also use to remove ON operator in the join if the same name in different tables

SELECT
	o.order_id,
    c.first_name
FROM orders o
JOIN customers c
	-- ON o.customer_id = c.customer_id
	USING (customer_id);

-- ..........................
SELECT
	o.order_id,
    c.first_name,
    sh.name  AS shipper
FROM orders o
JOIN customers c
	-- ON o.customer_id = c.customer_id
	USING (customer_id)
LEFT JOIN shippers sh
	USING (shipper_id);

-- ............................
USE sql_invoicing;

SELECT
	p.date,
    c.name AS client,
    p.amount,
    pm.name AS payment_method
FROM payments p
JOIN clients c USING (client_id)
JOIN payment_methods pm
	ON p.payment_method = pm.payment_method_id;
    
    
-- // NATURAL JOIN
USE sql_store;
SELECT
	o.order_id,
    c.first_name
FROM orders o
NATURAL JOIN customers c;

-- // CROSS JOIN

SELECT 
	c.first_name AS customer,
	p.name AS product
FROM customers c
CROSS JOIN products p
ORDER BY c.first_name;

-- // UNION 
-- //  using union we can combine record from the multiple quary 
SELECT 
	order_id,
    order_date,
    'Active' AS status
FROM orders
WHERE order_date >= '2019-01-01'
UNION
SELECT 
	order_id,
    order_date,
    'Archived' AS status
FROM orders
WHERE order_date < '2019-01-01';

-- //..............................

SELECT first_name
FROM customers
UNION
SELECT name FROM shippers;

-- //..............................
SELECT 
	customer_id, 
    first_name,
    points,
    'Bronze' AS type
FROM customers
WHERE points < 2000
UNION
SELECT
	customer_id,
    first_name,
    points,
    'Silver' AS type
FROM customers
WHERE points BETWEEN 2000 AND 3000
UNION
SELECT
	customer_id,
    first_name,
    points,
    'Gold' AS type
FROM customers
WHERE points > 3000
ORDER BY first_name;

-- // Column Attributes

-- // inserting value into table using INSERT INTO

INSERT INTO customers
VALUES (
	DEFAULT,
	'Lalit',
    'mali',
    '1998-04-26',
	NULL,
    'Undri',
    'Pune',
    'MH',
    DEFAULT);
-- // or we can insert in another way by ignoring those NULL and default values
INSERT INTO customers (first_name, last_name, birth_date, address, city, state)
VALUES (
	'Naresh',
    'mali',
    '1993-04-26',
    'Undri',
    'Pune',
    'MH');
select * from customers;

-- // inserting multiple rows
INSERT INTO shippers (name)
VALUES 
	('Shipper1'),
    ('Shipper2'),
    ('Shipper3');

select * from shippers;
	   	
-- // INSERTING Hierarchical Rows

-- // creating a copy of table
CREATE TABLE orders_archived AS
SELECT * FROM orders;

select * from orders_archived;


-- //  copy data from one table to another
INSERT INTO orders_archived
SELECT * FROM orders
WHERE order_date < '2019-01-01';

select * from orders_archived;

-- // updating data in single rows
select * from invoices;

UPDATE invoices
SET payment_total = 10, payment_date = '2019-03-01'
WHERE invoice_id = 1;

select * from invoices;

-- // ..............................
UPDATE invoices
SET
	payment_total = invoice_total*0.5,
    payment_date = due_date
WHERE invoice_id = 3;

select * from invoices;

-- // update multiple rows
UPDATE invoices
SET
	payment_total = invoice_total * 0.5,
    payment_date = due_date
WHERE client_id IN (3,4);

-- // exercise
-- write a sql statement to give any customers born before 1990 , 50 extra points
USE sql_store;

UPDATE customers
SET points = points + 50
WHERE birth_date < '1990-01-01';


-- // using subquries in update
UPDATE invoices
SET
	payment_total = invoice_total * 0.5,
    payment_date = due_date
WHERE client_id IN
			(SELECT client_id
			FROM clients
			WHERE state IN ('CA','NY'));
            
-- // exercise
use sql_store;

UPDATE orders
SET comments = 'Gold customers'
WHERE customer_id IN
			(SELECT customer_id
            FROM customers
            WHERE points > 3000);
            
-- // PS : - for multiple updation first change the safe update mode

-- // Deleting rows
-- 1
DELETE FROM invoices;  -- it will delete all the records

-- 2
DELETE FROM invoices
WHERE invoice_id = 1;

-- 3
DELETE FROM invoices
WHERE client_id =(
		SELECT *
        FROM clients
        WHERE name = 'Myworks');
        




		