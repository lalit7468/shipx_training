def isdivisible_by_all(n)
    for i in 2..20
        if n%i != 0
            return false
        end
    end
    return true
end

n = 20 
while true
    if isdivisible_by_all(n)
        break
    end
    n += 20
end

puts "smallest multiple number is #{n} "
