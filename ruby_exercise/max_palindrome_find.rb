def max_palindrome_find
    max_pali = 0
    product = 0
    
    for i in 100..999
        for j in 100..999
            product = i*j
            
            if(is_palindrome(product) && max_pali < product)
                max_pali = product
            end
        end    
    end
    return max_pali

end

def is_palindrome(n)
    np = n.to_s
    if np == np.reverse
        return true
    end
    return false
end


puts max_palindrome_find