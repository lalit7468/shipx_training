def find_largest_prime(n)
    mx_p = 0
    np = Math.sqrt(n)
    for i in 2..np
        while (n%i == 0 && n!=i)
            n = n/i
        end
        mx_p = n
    end        
    puts "largest prime factor : #{mx_p}"

end




# n=13195
n=600851475143
find_largest_prime(n)
